import { Routes } from '@angular/router';
import { BandejaideasComponent } from './pages/bandejaideas/bandejaideas.component';
import { LoginComponent } from './pages/login/login.component';
import { MaestrocategoriasComponent } from './pages/maestrocategorias/maestrocategorias.component';
import { MaestroconvocatoriasComponent } from './pages/maestroconvocatorias/maestroconvocatorias.component';
import { MaestroimpactoComponent } from './pages/maestroimpacto/maestroimpacto.component';
import { MaestrolineasComponent } from './pages/maestrolineas/maestrolineas.component';
import { MaestronuevacategoriaComponent } from './pages/maestronuevacategoria/maestronuevacategoria.component';
import { MaestronuevalineaComponent } from './pages/maestronuevalinea/maestronuevalinea.component';
import { MaestronuevaunidadComponent } from './pages/maestronuevaunidad/maestronuevaunidad.component';
import { MaestronuevauoComponent } from './pages/maestronuevauo/maestronuevauo.component';
import { MaestronuevoconvocatoriasComponent } from './pages/maestronuevoconvocatorias/maestronuevoconvocatorias.component';
import { MaestronuevoimpactoComponent } from './pages/maestronuevoimpacto/maestronuevoimpacto.component';
import { MaestronuevorolComponent } from './pages/maestronuevorol/maestronuevorol.component';
import { MaestronuevotipoproyectoComponent } from './pages/maestronuevotipoproyecto/maestronuevotipoproyecto.component';
import { MaestrorolesComponent } from './pages/maestroroles/maestroroles.component';
import { MaestrotipoproyectosComponent } from './pages/maestrotipoproyectos/maestrotipoproyectos.component';
import { MaestrounidadesComponent } from './pages/maestrounidades/maestrounidades.component';
import { MaestrounidadesorganizativasComponent } from './pages/maestrounidadesorganizativas/maestrounidadesorganizativas.component';
import { MaestrousuariosComponent } from './pages/maestrousuarios/maestrousuarios.component';
import { MaestrousuariosnuevoComponent } from './pages/maestrousuariosnuevo/maestrousuariosnuevo.component';
import { ModificoproyectosComponent } from './pages/modificoproyectos/modificoproyectos.component';
import { RegistroproyectosComponent } from './pages/registroproyectos/registroproyectos.component';
import { ResultadoscomiteComponent } from './pages/resultadoscomite/resultadoscomite.component';
import { RevisioncomiteComponent } from './pages/revisioncomite/revisioncomite.component';
import { RevisionproyectosComponent } from './pages/revisionproyectos/revisionproyectos.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';

export const ROUTES: Routes = [
    { path: 'login', component: LoginComponent},
    { path: 'welcome', component: WelcomeComponent},
    { path: 'bandeja-de-ideas', component: BandejaideasComponent},
    { path: 'registrar-proyectos', component: RegistroproyectosComponent},
    { path: 'modificar-proyectos/:id', component: ModificoproyectosComponent},
    { path: 'revisar-proyectos/:id', component: RevisionproyectosComponent},
    { path: 'programar-comite-proyectos/:id', component: RevisioncomiteComponent},
    { path: 'revisar-comite-proyectos/:id', component: ResultadoscomiteComponent},
    { path: 'maestros/convocatorias', component: MaestroconvocatoriasComponent},
    { path: 'maestros/convocatorias/nuevo', component: MaestronuevoconvocatoriasComponent},
    { path: 'maestros/tipo-proyectos', component: MaestrotipoproyectosComponent},
    { path: 'maestros/tipo-proyectos/nuevo', component: MaestronuevotipoproyectoComponent},
    { path: 'maestros/unidades', component: MaestrounidadesComponent},
    { path: 'maestros/unidades/nuevo', component: MaestronuevaunidadComponent},
    { path: 'maestros/lineas', component: MaestrolineasComponent},
    { path: 'maestros/lineas/nuevo', component: MaestronuevalineaComponent},
    { path: 'maestros/categorias', component: MaestrocategoriasComponent},
    { path: 'maestros/categorias/nuevo', component: MaestronuevacategoriaComponent},
    { path: 'maestros/impacto', component: MaestroimpactoComponent},
    { path: 'maestros/impacto/nuevo', component: MaestronuevoimpactoComponent},
    { path: 'maestros/roles', component: MaestrorolesComponent},
    { path: 'maestros/roles/nuevo', component: MaestronuevorolComponent},
    { path: 'maestros/unidades-organizativas', component: MaestrounidadesorganizativasComponent},
    { path: 'maestros/unidades-organizativas/nuevo', component: MaestronuevauoComponent},
    { path: 'maestros/usuarios', component: MaestrousuariosComponent},
    { path: 'maestros/usuarios/nuevo', component: MaestrousuariosnuevoComponent},
    { path: '', pathMatch: 'full', redirectTo: 'login' },
    { path: '**', pathMatch: 'full', redirectTo: 'login' }
];
