import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestronuevacategoria',
  templateUrl: './maestronuevacategoria.component.html',
  styles: []
})
export class MaestronuevacategoriaComponent implements OnInit {

  nuevaCategoriaId: string = '';
  nuevaCategoriaDescripcion: string = '';

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
  }

  crearNuevaCategoria(){
    const dataPost = {
      id: parseInt(this.nuevaCategoriaId),
      descripcion: this.nuevaCategoriaDescripcion
    };

    console.log(dataPost);

    this.api.getNuevaCategoria(dataPost).subscribe((data: any) => {
      console.log(data);
      if (data.id == this.nuevaCategoriaId) {
        swal.fire({
          title: 'Exito',
          text: "Categoria creada correctamente",
          icon: 'success',
        }).then(() => {
          this.router.navigate(['maestros/categorias']);
        });
      } else {
        swal.fire({
          title: 'Error',
          text: "Hubo un error al crear la categoria, intentelo nuevamente",
          icon: 'error',
        });
      }
    });
  }

}
