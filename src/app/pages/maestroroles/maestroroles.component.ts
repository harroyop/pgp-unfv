import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestroroles',
  templateUrl: './maestroroles.component.html',
  styles: []
})
export class MaestrorolesComponent implements OnInit {

  roles: any = [];

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataInicial();
  }

  cargaDataInicial(){
    this.api.getData('pgp_roles').subscribe((data: any) => {
      this.roles = data;
    });
  }

  deleteRol(id: number){
    if(id == 1 || id == 2 || id == 3){
      swal.fire({
        title: 'Informacion',
        text: "Este es un rol por defecto y no se puede eliminar.",
        icon: 'info',
      });
    }else{
      this.api.delData('pgp_roles/' + id).subscribe((data: any) => {
        if(data == id){
          swal.fire({
            title: 'Exito',
            text: "Rol eliminado correctamente.",
            icon: 'success',
          }).then(() => {
            location.reload();
          });
        }else{
          swal.fire({
            title: 'Error',
            text: "Hubo un error al eliminar el rol.",
            icon: 'error',
          });  
        }
      });
    }
  }

}
