import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';
import { UpperCasePipe } from '@angular/common';

@Component({
  selector: 'app-resultadoscomite',
  templateUrl: './resultadoscomite.component.html',
  styles: []
})
export class ResultadoscomiteComponent implements OnInit {

  convocatoriastmp: any = [];
  convocatorias: any = [];
  unidadtmp: any = [];
  unidad: any = [];
  lineatmp: any = [];
  linea: any = [];
  categoriatmp: any = [];
  categoria: any = [];
  impactotmp: any = [];
  impacto: any = [];
  documentotmp: any = [];
  documento: any = [];
  usuariostmp: any = [];
  usuarios: any = [];
  proyectotmp: any = [];
  proyecto: any = [];
  programaciontmp: any = [];
  programacion: any = [];

  proyectoId: string = '';
  proyectoCodProyecto: string = '';
  proyectoIdConvocatoria: string = '';
  proyectoConvocatoria: string = '';
  proyectoNombre: string = '';
  proyectoDescripcion: string = '';
  proyectoIdUnidad: string = '';
  proyectoUnidad: string = '';
  proyectoIdLinea: string = '';
  proyectoLinea: string = '';
  proyectoIdCategoria: string = '';
  proyectoCategoria: string = '';
  proyectoIdImpacto: string = '';
  proyectoImpacto: string = '';
  proyectoLider: string = '';
  proyectoLiderNombre: string = '';
  proyectoAsesor: string = '';
  proyectoEquipo: any = [];
  proyectoIdDocumentoProyecto: string = '0';
  proyectoDocumentoProyecto: string = '0';
  proyectoFechaRegistro: string = '';
  proyectoEstado: string = 'REGISTRADO';
  proyectoEtapa: string = 'REGISTRO DE PROYECTO';
  proyectoResultado: string = 'PENDIENTE';
  proyectoCriterios: any = [];

  resultadoComiteId: string = (Math.floor(Math.random() * (999 - 1)) + 1).toString();
  resultadoComiteResultado: string = '';
  resultadoComiteMotivo: string = '';
  resultadoComiteCalificacion: number = 0;
  resultadoComiteCriterios: any = [];

  modalRef: BsModalRef;

  constructor(private route: ActivatedRoute, private router: Router, private api: ApiService, private appComponent: AppComponent, private modalService: BsModalService) {
    this.appComponent.login = false;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  ngOnInit() {
    this.proyectoId = this.route.snapshot.paramMap.get('id');
    this.cargaDataInicial();
  }

  cargaDataInicial(){
    this.api.getData('pgp_convocatorias').subscribe((data: any) => {
      this.convocatorias = data;
      
      this.api.getData('pgp_unidades').subscribe((data2: any) => {
        this.unidad = data2;

        this.api.getData('pgp_lineas').subscribe((data3: any) => {
          this.linea = data3;

          this.api.getData('pgp_categoria').subscribe((data4: any) => {
            this.categoria = data4;

            this.api.getData('pgp_impacto').subscribe((data5: any) => {
              this.impacto = data5;
              
              this.api.getData('pgp_documento_proyectos').subscribe((data6: any) => {
                this.documento = data6;

                this.api.getData('pgp_usuarios').subscribe((data7: any) => {
                  this.usuarios = data7;
  
                  this.api.getData('pgp_programacion_comites').subscribe((data9: any) => {
                    this.programacion = data9;

                    this.api.getData('pgp_proyectos/' + this.proyectoId).subscribe((data8: any) => {
                      console.log(data8);
                      this.proyectotmp = data8;
  
                      this.proyectoNombre = this.proyectotmp.nombre;
                      this.proyectoDescripcion = this.proyectotmp.descripcion;
                      this.proyectoAsesor = this.proyectotmp.asesor;
                      this.proyectoFechaRegistro = this.proyectotmp.fecha_registro;
    
                      this.convocatorias.forEach(element => {                        
                        if(this.proyectotmp.id_convocatoria == element.id){                          
                          this.proyectoConvocatoria = element.descripcion;
                        }
                      });
  
                      this.unidad.forEach(element2 => {
                        if(this.proyectotmp.id_unidad == element2.id){
                          this.proyectoUnidad = element2.descripcion;
                        }
                      });
  
                      this.linea.forEach(element3 => {
                        if(this.proyectotmp.id_linea == element3.id){
                          this.proyectoLinea = element3.descripcion;
                        }
                      });
  
                      this.categoria.forEach(element4 => {
                        if(this.proyectotmp.id_categoria == element4.id){
                          this.proyectoCategoria = element4.descripcion;
                        }
                      });
  
                      this.impacto.forEach(element5 => {
                        if(this.proyectotmp.id_impacto == element5.id){
                          this.proyectoImpacto = element5.descripcion;
                        }
                      });
  
                      this.documento.forEach(element6 => {
                        if(this.proyectotmp.id_documento_proyecto == element6.id){
                          this.proyectoDocumentoProyecto = element6.ruta;
                        }
                      });
  
                      this.usuarios.forEach(element7 => {
                        if(this.proyectotmp.lider == element7.id){
                          this.proyectoLiderNombre = element7.nombres + ' ' + element7.apellidos;
                        }
                      });

                      this.programacion.forEach(element => {                        
                        if(this.proyectotmp.id == element.id_proyecto){
                          this.proyectoCriterios = JSON.parse(element.criterios);
                        }
                      });
                      
                    });

                  });
                  
                });                
                
              });
              
            });
            
          });
          
        });
        
      });

    });

  }

  evaluarProyecto(){
    const dataPost = {
      id: parseInt(this.resultadoComiteId),
      id_proyecto: parseInt(this.proyectoId),
      resultado: this.resultadoComiteResultado,
      motivo: this.resultadoComiteMotivo,
      criterios: JSON.stringify(this.resultadoComiteCriterios),
      calificacion: this.resultadoComiteCalificacion
    }

    console.log(dataPost);

    this.api.getEvaluaComite(dataPost).subscribe((data: any) => {
      if (data.id == this.resultadoComiteId) {
        this.actualizaProyecto();
        swal.fire({
          title: 'Exito',
          text: "Proyecto evaluado correctamente",
          icon: 'success',
        }).then(() => {
          this.router.navigate(['bandeja-de-ideas']);
        });
      } else {
        swal.fire({
          title: 'Error',
          text: "Hubo un error al evaluar el proyecto, intentelo nuevamente",
          icon: 'error',
        });
      }
    });

  }

  actualizaProyecto(){
    this.proyectotmp.estado = this.resultadoComiteResultado.toUpperCase();

    if(this.resultadoComiteResultado == 'Priorizado'){
      this.proyectotmp.etapa = 'APROBADO POR COMITE';
    }else{
      this.proyectotmp.etapa = 'REGISTRO DE PROYECTO';
    }
    
    this.proyectotmp.resultado = this.resultadoComiteMotivo;

    const dataPost = this.proyectotmp;

    console.log(dataPost);

    this.api.getUpdateProyecto(dataPost, this.proyectoId).subscribe((data: any) => {
      console.log('Proyecto actualizado:', data);
    });
  }

  validaMontoCriterio(valor){    
    if((parseInt(valor.target.value) < 0 ) || (parseInt(valor.target.value) > 50)){
      swal.fire({
        title: 'Error',
        text: "El puntaje maximo para el criterio es de 50.",
        icon: 'error',
      }).then(() => {
        valor.target.value = '';
        this.resultadoComiteCalificacion = 0;
      });
    }

    this.sumaCriterios();
  }

  sumaCriterios(){
    this.resultadoComiteCalificacion = 0
    let criterios_sumar = document.querySelectorAll('.single-criterio');

    [].forEach.call(criterios_sumar, (e)=>{
      if(e.value != ''){
        this.resultadoComiteCalificacion = ((this.resultadoComiteCalificacion * 1) + (e.value * 1)) * 1;
      }      
     });
  }

}
