import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestrocategorias',
  templateUrl: './maestrocategorias.component.html',
  styles: []
})
export class MaestrocategoriasComponent implements OnInit {

  categoria: any = [];

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataInicial();
  }

  cargaDataInicial(){
    this.api.getData('pgp_categoria').subscribe((data: any) => {
      this.categoria = data;
    });
  }

  deleteCategoria(id: number){
    this.api.delData('pgp_categoria/' + id).subscribe((data: any) => {
      if(data == id){
        swal.fire({
          title: 'Exito',
          text: "Categoria eliminada correctamente.",
          icon: 'success',
        }).then(() => {
          location.reload();
        });
      }else{
        swal.fire({
          title: 'Error',
          text: "Hubo un error al eliminar la categoria.",
          icon: 'error',
        });  
      }
    });
  }

}
