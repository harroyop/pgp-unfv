import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestrousuarios',
  templateUrl: './maestrousuarios.component.html',
  styles: []
})
export class MaestrousuariosComponent implements OnInit {

  uniorgtmp: any = [];
  uniorg: any = [];
  roltmp: any = [];
  rol: any = [];
  usuariostmp: any = [];
  usuarios: any = [];

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataInicial();
  }

  cargaDataInicial(){
    let tmpuniorg = '';
    let tmprol = '';

    this.api.getData('pgp_unidades_organizativas').subscribe((data: any) => {
      this.uniorg = data;
      
      this.api.getData('pgp_roles').subscribe((data2: any) => {
        this.rol = data2;

        this.api.getData('pgp_usuarios').subscribe((data3: any) => {
          this.usuariostmp = data3;

          this.usuariostmp.forEach(elem => {

            this.uniorg.forEach(element1 => {
              if(elem.id_unidad_organizativa == element1.id){
                tmpuniorg = element1.descripcion;
              }
            });

            this.rol.forEach(element2 => { 
              if(elem.id_rol == element2.id){
                tmprol = element2.descripcion;
              }
            });
  
            this.usuarios.push({
              id: elem.id,
              cod_usuario: elem.cod_usuario,
              nombres: elem.nombres,
              apellidos: elem.apellidos,
              unidad_organizativa: tmpuniorg,
              telefono: elem.telefono,
              correo: elem.correo,
              contrasena: elem.contrasena,
              rol: tmprol
            });
          });  
          
        });
        
      });
    });

    console.log(this.usuarios);
  }

  deleteUsuario(id: number){
    this.api.delData('pgp_usuarios/' + id).subscribe((data: any) => {
      if(data == id){
        swal.fire({
          title: 'Exito',
          text: "Usuario eliminado correctamente.",
          icon: 'success',
        }).then(() => {
          location.reload();
        });
      }else{
        swal.fire({
          title: 'Error',
          text: "Hubo un error al eliminar el usuario.",
          icon: 'error',
        });  
      }
    });
  }

}
