import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestroimpacto',
  templateUrl: './maestroimpacto.component.html',
  styles: []
})
export class MaestroimpactoComponent implements OnInit {

  impacto: any = [];

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataInicial();
  }

  cargaDataInicial(){
    this.api.getData('pgp_impacto').subscribe((data: any) => {
      this.impacto = data;
    });
  }

  deleteImpacto(id: number){
    this.api.delData('pgp_impacto/' + id).subscribe((data: any) => {
      if(data == id){
        swal.fire({
          title: 'Exito',
          text: "Impacto eliminado correctamente.",
          icon: 'success',
        }).then(() => {
          location.reload();
        });
      }else{
        swal.fire({
          title: 'Error',
          text: "Hubo un error al eliminar el impacto.",
          icon: 'error',
        });  
      }
    });
  }

}
