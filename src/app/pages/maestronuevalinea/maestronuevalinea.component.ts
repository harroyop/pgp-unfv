import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestronuevalinea',
  templateUrl: './maestronuevalinea.component.html',
  styles: []
})
export class MaestronuevalineaComponent implements OnInit {

  unidades: any = [];
  nuevaLineaId: string = '';
  nuevaLineaDescripcion: string = '';
  nuevaLineaIdUnidad: string = '';

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataUnidades();
  }

  cargaDataUnidades(){
    this.api.getData('pgp_unidades').subscribe((data: any) => {
      this.unidades = data;
    });
  }

  crearNuevaLinea(){
    const dataPost = {
      id: parseInt(this.nuevaLineaId),
      descripcion: this.nuevaLineaDescripcion,
      id_unidad: parseInt(this.nuevaLineaIdUnidad)
    };

    console.log(dataPost);

    this.api.getNuevaLinea(dataPost).subscribe((data: any) => {
      console.log(data);
      if (data.id == this.nuevaLineaId) {
        swal.fire({
          title: 'Exito',
          text: "Linea creada correctamente",
          icon: 'success',
        }).then(() => {
          this.router.navigate(['maestros/lineas']);
        });
      } else {
        swal.fire({
          title: 'Error',
          text: "Hubo un error al crear la linea, intentelo nuevamente",
          icon: 'error',
        });
      }
    });
  }

}
