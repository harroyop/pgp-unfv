import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestronuevorol',
  templateUrl: './maestronuevorol.component.html',
  styles: []
})
export class MaestronuevorolComponent implements OnInit {

  nuevoRolId: string = '';
  nuevoRolDescripcion: string = '';

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
  }

  crearNuevoRol(){
    const dataPost = {
      id: parseInt(this.nuevoRolId),
      descripcion: this.nuevoRolDescripcion
    };

    console.log(dataPost);

    this.api.getNuevoRol(dataPost).subscribe((data: any) => {
      console.log(data);
      if (data.id == this.nuevoRolId) {
        swal.fire({
          title: 'Exito',
          text: "Rol creado correctamente",
          icon: 'success',
        }).then(() => {
          this.router.navigate(['maestros/roles']);
        });
      } else {
        swal.fire({
          title: 'Error',
          text: "Hubo un error al crear el rol, intentelo nuevamente",
          icon: 'error',
        });
      }
    });
  }

}
