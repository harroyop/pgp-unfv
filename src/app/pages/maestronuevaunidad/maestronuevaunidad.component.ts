import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestronuevaunidad',
  templateUrl: './maestronuevaunidad.component.html',
  styles: []
})
export class MaestronuevaunidadComponent implements OnInit {

  nuevaUnidadId: string = '';
  nuevaUnidadDescripcion: string = '';

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
  }

  crearNuevaUnidad(){
    const dataPost = {
      id: parseInt(this.nuevaUnidadId),
      descripcion: this.nuevaUnidadDescripcion
    };

    console.log(dataPost);

    this.api.getNuevaUnidad(dataPost).subscribe((data: any) => {
      console.log(data);
      if (data.id == this.nuevaUnidadId) {
        swal.fire({
          title: 'Exito',
          text: "Unidad creada correctamente",
          icon: 'success',
        }).then(() => {
          this.router.navigate(['maestros/unidades']);
        });
      } else {
        swal.fire({
          title: 'Error',
          text: "Hubo un error al crear la unidad, intentelo nuevamente",
          icon: 'error',
        });
      }
    });
  }

}
