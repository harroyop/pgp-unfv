import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-revisioncomite',
  templateUrl: './revisioncomite.component.html',
  styles: []
})
export class RevisioncomiteComponent implements OnInit {

  convocatoriastmp: any = [];
  convocatorias: any = [];
  unidadtmp: any = [];
  unidad: any = [];
  lineatmp: any = [];
  linea: any = [];
  categoriatmp: any = [];
  categoria: any = [];
  impactotmp: any = [];
  impacto: any = [];
  documentotmp: any = [];
  documento: any = [];
  usuariostmp: any = [];
  usuarios: any = [];
  proyectotmp: any = [];
  proyecto: any = [];

  proyectoId: string = '';
  proyectoCodProyecto: string = '';
  proyectoIdConvocatoria: string = '';
  proyectoConvocatoria: string = '';
  proyectoNombre: string = '';
  proyectoDescripcion: string = '';
  proyectoIdUnidad: string = '';
  proyectoUnidad: string = '';
  proyectoIdLinea: string = '';
  proyectoLinea: string = '';
  proyectoIdCategoria: string = '';
  proyectoCategoria: string = '';
  proyectoIdImpacto: string = '';
  proyectoImpacto: string = '';
  proyectoLider: string = '';
  proyectoLiderNombre: string = '';
  proyectoAsesor: string = '';
  proyectoEquipo: any = [];
  proyectoIdDocumentoProyecto: string = '0';
  proyectoDocumentoProyecto: string = '0';
  proyectoFechaRegistro: string = '';
  proyectoEstado: string = 'REGISTRADO';
  proyectoEtapa: string = 'REGISTRO DE PROYECTO';
  proyectoResultado: string = 'PENDIENTE';

  registroComiteId: string = (Math.floor(Math.random() * (999 - 1)) + 1).toString();
  registroComiteFecha: string = '';
  registroComiteLugar: string = '';
  registroComiteParticipantes: any = [];
  registroComiteCriterio: any = [];

  modalRef: BsModalRef;

  constructor(private route: ActivatedRoute, private router: Router, private api: ApiService, private appComponent: AppComponent, private modalService: BsModalService) {
    this.appComponent.login = false;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  ngOnInit() {
    this.proyectoId = this.route.snapshot.paramMap.get('id');
    this.cargaDataInicial();
  }

  cargaDataInicial(){
    this.api.getData('pgp_convocatorias').subscribe((data: any) => {
      this.convocatorias = data;
      
      this.api.getData('pgp_unidades').subscribe((data2: any) => {
        this.unidad = data2;

        this.api.getData('pgp_lineas').subscribe((data3: any) => {
          this.linea = data3;

          this.api.getData('pgp_categoria').subscribe((data4: any) => {
            this.categoria = data4;

            this.api.getData('pgp_impacto').subscribe((data5: any) => {
              this.impacto = data5;
              
              this.api.getData('pgp_documento_proyectos').subscribe((data6: any) => {
                this.documento = data6;

                this.api.getData('pgp_usuarios').subscribe((data7: any) => {
                  this.usuarios = data7;
  
                  this.api.getData('pgp_proyectos/' + this.proyectoId).subscribe((data8: any) => {
                    console.log(data8);
                    this.proyectotmp = data8;

                    this.proyectoNombre = this.proyectotmp.nombre;
                    this.proyectoDescripcion = this.proyectotmp.descripcion;
                    this.proyectoAsesor = this.proyectotmp.asesor;
                    this.proyectoFechaRegistro = this.proyectotmp.fecha_registro;
  
                    this.convocatorias.forEach(element => {                        
                      if(this.proyectotmp.id_convocatoria == element.id){                          
                        this.proyectoConvocatoria = element.descripcion;
                      }
                    });

                    this.unidad.forEach(element2 => {
                      if(this.proyectotmp.id_unidad == element2.id){
                        this.proyectoUnidad = element2.descripcion;
                      }
                    });

                    this.linea.forEach(element3 => {
                      if(this.proyectotmp.id_linea == element3.id){
                        this.proyectoLinea = element3.descripcion;
                      }
                    });

                    this.categoria.forEach(element4 => {
                      if(this.proyectotmp.id_categoria == element4.id){
                        this.proyectoCategoria = element4.descripcion;
                      }
                    });

                    this.impacto.forEach(element5 => {
                      if(this.proyectotmp.id_impacto == element5.id){
                        this.proyectoImpacto = element5.descripcion;
                      }
                    });

                    this.documento.forEach(element6 => {
                      if(this.proyectotmp.id_documento_proyecto == element6.id){
                        this.proyectoDocumentoProyecto = element6.ruta;
                      }
                    });

                    this.usuarios.forEach(element7 => {
                      if(this.proyectotmp.lider == element7.id){
                        this.proyectoLiderNombre = element7.nombres + ' ' + element7.apellidos;
                      }
                    });
                    
                  });                  
                  
                });                
                
              });
              
            });
            
          });
          
        });
        
      });

    });

  }

  agregaParticipante(){
    let nombres = document.getElementById('agregaParticipante') as HTMLInputElement;

    this.registroComiteParticipantes.push({
      nombre: nombres.value,
    });

    nombres.value = '';
  }

  agregaCriterio(){
    let nombres = document.getElementById('agregaCriterio') as HTMLInputElement;

    this.registroComiteCriterio.push({
      nombre: nombres.value,
    });

    console.log(this.registroComiteCriterio);
    nombres.value = '';
  }

  
  eliminaParticipante(nombre){
    for(var i = 0; i < this.registroComiteParticipantes.length; i++){
      if(this.registroComiteParticipantes[i].nombre == nombre){
        this.registroComiteParticipantes.splice(i, 1);
        
        swal.fire({
          title: 'Exito',
          text: 'Participante eliminado correctamente.',
          icon: 'success',
        });
      }
    }
  }

  eliminaCriterio(nombre){
    for(var i = 0; i < this.registroComiteCriterio.length; i++){
      if(this.registroComiteCriterio[i].nombre == nombre){
        this.registroComiteCriterio.splice(i, 1);
        
        swal.fire({
          title: 'Exito',
          text: 'Criterio eliminado correctamente.',
          icon: 'success',
        });
      }
    }
  }

  programaProyecto(){
    const dataPost = {
      id: parseInt(this.registroComiteId),
      id_proyecto: parseInt(this.proyectoId),
      fecha: this.registroComiteFecha,
      lugar: this.registroComiteLugar,
      participantes: JSON.stringify(this.registroComiteParticipantes),
      criterios: JSON.stringify(this.registroComiteCriterio)
    }

    console.log(dataPost);

    this.api.getNuevoComite(dataPost).subscribe((data: any) => {
      if (data.id == this.registroComiteId) {
        this.actualizaProyecto();
        swal.fire({
          title: 'Exito',
          text: "Proyecto programado correctamente",
          icon: 'success',
        }).then(() => {
          this.router.navigate(['bandeja-de-ideas']);
        });
      } else {
        swal.fire({
          title: 'Error',
          text: "Hubo un error al programar el proyecto, intentelo nuevamente",
          icon: 'error',
        });
      }
    });

  }

  actualizaProyecto(){

    this.proyectotmp.estado = 'PROGRAMADO';
    this.proyectotmp.etapa = 'EN COMITE';
    this.proyectotmp.resultado = 'Fecha: ' + this.registroComiteFecha + ' | Lugar: ' + this.registroComiteLugar;

    const dataPost = this.proyectotmp;

    console.log(dataPost);

    this.api.getUpdateProyecto(dataPost, this.proyectoId).subscribe((data: any) => {
      console.log('Proyecto actualizado:', data);
    });
  }

}
