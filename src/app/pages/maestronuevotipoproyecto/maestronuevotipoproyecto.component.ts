import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestronuevotipoproyecto',
  templateUrl: './maestronuevotipoproyecto.component.html',
  styles: []
})
export class MaestronuevotipoproyectoComponent implements OnInit {

  nuevoTipoProyectoId: string = '';
  nuevoTipoProyectoDescripcion: string = '';
  nuevoTipoProyectoRequisitos: string = '';

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
  }

  crearNuevoTipoProyecto(){
    const dataPost = {
      id: parseInt(this.nuevoTipoProyectoId),
      descripcion: this.nuevoTipoProyectoDescripcion,
      requisitos: this.nuevoTipoProyectoRequisitos
    };

    console.log(dataPost);

    this.api.getNuevoTipoProyecto(dataPost).subscribe((data: any) => {
      console.log(data);
      if (data.id == this.nuevoTipoProyectoId) {
        swal.fire({
          title: 'Exito',
          text: "Tipo Proyecto creado correctamente",
          icon: 'success',
        }).then(() => {
          this.router.navigate(['maestros/tipo-proyectos']);
        });
      } else {
        swal.fire({
          title: 'Error',
          text: "Hubo un error al crear el Tipo Proyecto, intentelo nuevamente",
          icon: 'error',
        });
      }
    });
  }

}
