import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestrounidadesorganizativas',
  templateUrl: './maestrounidadesorganizativas.component.html',
  styles: []
})
export class MaestrounidadesorganizativasComponent implements OnInit {

  unidades: any = [];

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataInicial();
  }

  cargaDataInicial(){
    this.api.getData('pgp_unidades_organizativas').subscribe((data: any) => {
      this.unidades = data;
    });
  }

  deleteUO(id: number){
    this.api.delData('pgp_unidades_organizativas/' + id).subscribe((data: any) => {
      if(data == id){
        swal.fire({
          title: 'Exito',
          text: "Unidad eliminada correctamente.",
          icon: 'success',
        }).then(() => {
          location.reload();
        });
      }else{
        swal.fire({
          title: 'Error',
          text: "Hubo un error al eliminar esta unidad.",
          icon: 'error',
        });  
      }
    });
  }

}
