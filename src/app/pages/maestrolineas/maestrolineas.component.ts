import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestrolineas',
  templateUrl: './maestrolineas.component.html',
  styles: []
})
export class MaestrolineasComponent implements OnInit {

  lineastmp: any = [];
  lineas: any = [];
  unidades: any = [];

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataInicial();
  }

  cargaDataInicial(){
    let tmpunidad = '';

    this.api.getData('pgp_unidades').subscribe((data: any) => {
      this.unidades = data;
      
      this.api.getData('pgp_lineas').subscribe((data2: any) => {
        this.lineastmp = data2;

        this.lineastmp.forEach(elem => {

          this.unidades.forEach(element => {
            if(elem.id == element.id){
              tmpunidad = element.descripcion;
            }            
          });

          this.lineas.push({
            id: elem.id,
            descripcion: elem.descripcion,
            unidad: tmpunidad
          });
        });
      });
    });

    console.log(this.lineas);
  }

  deleteLinea(id: number){
    this.api.delData('pgp_lineas/' + id).subscribe((data: any) => {
      if(data == id){
        swal.fire({
          title: 'Exito',
          text: "Linea eliminada correctamente.",
          icon: 'success',
        }).then(() => {
          location.reload();
        });
      }else{
        swal.fire({
          title: 'Error',
          text: "Hubo un error al eliminar la linea.",
          icon: 'error',
        });  
      }
    });
  }

}
