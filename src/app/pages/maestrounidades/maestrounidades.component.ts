import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestrounidades',
  templateUrl: './maestrounidades.component.html',
  styles: []
})
export class MaestrounidadesComponent implements OnInit {

  unidades: any = [];

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataInicial();
  }

  cargaDataInicial(){
    this.api.getData('pgp_unidades').subscribe((data: any) => {
      this.unidades = data;
    });
  }

  deleteUnidad(id: number){
    this.api.delData('pgp_unidades/' + id).subscribe((data: any) => {
      if(data == id){
        swal.fire({
          title: 'Exito',
          text: "Unidad eliminada correctamente.",
          icon: 'success',
        }).then(() => {
          location.reload();
        });
      }else{
        swal.fire({
          title: 'Error',
          text: "Hubo un error al eliminar la unidad.",
          icon: 'error',
        });  
      }
    });
  }

}
