import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestroconvocatorias',
  templateUrl: './maestroconvocatorias.component.html',
  styles: []
})
export class MaestroconvocatoriasComponent implements OnInit {

  tipoproytmp: any = [];
  tipoproy: any = [];
  unidadtmp: any = [];
  unidad: any = [];
  convocatoriastmp: any = [];
  convocatorias: any = [];

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataInicial();
  }

  cargaDataInicial(){
    let tmpunidad = '';
    let tmptipoproy = '';

    this.api.getData('pgp_tipo_proyectos').subscribe((data: any) => {
      this.tipoproy = data;
      
      this.api.getData('pgp_unidades').subscribe((data2: any) => {
        this.unidad = data2;

        this.api.getData('pgp_convocatorias').subscribe((data3: any) => {
          this.convocatoriastmp = data3;

          this.convocatoriastmp.forEach(elem => {

            this.tipoproy.forEach(element1 => {
              if(elem.id_tipo_proyecto == element1.id){
                tmptipoproy = element1.descripcion;
              }
            });

            this.unidad.forEach(element2 => { 
              if(elem.id_unidad == element2.id){
                tmpunidad = element2.descripcion;
              }
            });
  
            this.convocatorias.push({
              id: elem.id,
              descripcion: elem.descripcion,
              tipo_proyecto: tmptipoproy,
              unidad: tmpunidad,
              vigencia: elem.vigencia
            });
          });  
          
        });
        
      });
    });

    console.log(this.convocatorias);
  }

  deleteConvocatoria(id: number){
    this.api.delData('pgp_convocatorias/' + id).subscribe((data: any) => {
      if(data == id){
        swal.fire({
          title: 'Exito',
          text: "Convocatoria eliminada correctamente.",
          icon: 'success',
        }).then(() => {
          location.reload();
        });
      }else{
        swal.fire({
          title: 'Error',
          text: "Hubo un error al eliminar la convocatoria.",
          icon: 'error',
        });  
      }
    });
  }

}
