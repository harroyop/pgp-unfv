import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-bandejaideas',
  templateUrl: './bandejaideas.component.html',
  styles: []
})
export class BandejaideasComponent implements OnInit {

  convocatoriastmp: any = [];
  convocatorias: any = [];
  unidadtmp: any = [];
  unidad: any = [];
  lineatmp: any = [];
  linea: any = [];
  categoriatmp: any = [];
  categoria: any = [];
  impactotmp: any = [];
  impacto: any = [];
  documentotmp: any = [];
  documento: any = [];
  usuariostmp: any = [];
  usuarios: any = [];
  proyectotmp: any = [];
  proyecto: any = [];
  

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataInicial();
  }

  cargaDataInicial(){
    let tmpconvocatorias = '';
    let tmpunidad = '';
    let tmplinea = '';
    let tmpcategoria = '';
    let tmpimpacto = '';
    let tmpdocumento = '';
    let tmpusuario = '';
    let tmpliderid = '';

    this.api.getData('pgp_convocatorias').subscribe((data: any) => {
      this.convocatorias = data;
      
      this.api.getData('pgp_unidades').subscribe((data2: any) => {
        this.unidad = data2;

        this.api.getData('pgp_lineas').subscribe((data3: any) => {
          this.linea = data3;

          this.api.getData('pgp_categoria').subscribe((data4: any) => {
            this.categoria = data4;

            this.api.getData('pgp_impacto').subscribe((data5: any) => {
              this.impacto = data5;
              
              this.api.getData('pgp_documento_proyectos').subscribe((data6: any) => {
                this.documento = data6;

                this.api.getData('pgp_usuarios').subscribe((data7: any) => {
                  this.usuarios = data7;
  
                  this.api.getData('pgp_proyectos').subscribe((data8: any) => {
                    this.proyectotmp = data8;
  
                    this.proyectotmp.forEach(elem => {
  
                      this.convocatorias.forEach(element => {
                        if(elem.id_convocatoria == element.id){
                          tmpconvocatorias = element.descripcion;
                        }
                      });
  
                      this.unidad.forEach(element2 => {
                        if(elem.id_unidad == element2.id){
                          tmpunidad = element2.descripcion;
                        }
                      });

                      this.linea.forEach(element3 => {
                        if(elem.id_linea == element3.id){
                          tmplinea = element3.descripcion;
                        }
                      });

                      this.categoria.forEach(element4 => {
                        if(elem.id_categoria == element4.id){
                          tmpcategoria = element4.descripcion;
                        }
                      });

                      this.impacto.forEach(element5 => {
                        if(elem.id_impacto == element5.id){
                          tmpimpacto = element5.descripcion;
                        }
                      });

                      this.documento.forEach(element6 => {
                        if(elem.id_documento_proyecto == element6.id){
                          tmpdocumento = element6.ruta;
                        }
                      });

                      this.usuarios.forEach(element7 => {
                        if(elem.lider == element7.id){
                          tmpliderid = element7.id;
                          tmpusuario = element7.nombres + ' ' + element7.apellidos;
                        }
                      });

                      if(localStorage.getItem('id_rol') == '2'){

                        if(elem.lider == localStorage.getItem('id_usuario')){
                          this.proyecto.push({
                            id: elem.id,
                            cod_proyecto: elem.cod_proyecto,
                            convocatoria: tmpconvocatorias,
                            nombre: elem.nombre,
                            descripcion: elem.descripcion,
                            unidad: tmpunidad,
                            linea: tmplinea,
                            categoria: tmpcategoria,
                            impacto: tmpimpacto,
                            lider_id: tmpliderid,
                            lider: tmpusuario,
                            asesor: elem.asesor,
                            equipo: elem.equipo,
                            documento_proyecto: tmpdocumento,
                            fecha_registro: elem.fecha_registro,
                            estado: elem.estado,
                            etapa: elem.etapa,
                            resultado: elem.resultado
                          });
                        }

                      }else{

                        this.proyecto.push({
                          id: elem.id,
                          cod_proyecto: elem.cod_proyecto,
                          convocatoria: tmpconvocatorias,
                          nombre: elem.nombre,
                          descripcion: elem.descripcion,
                          unidad: tmpunidad,
                          linea: tmplinea,
                          categoria: tmpcategoria,
                          impacto: tmpimpacto,
                          lider_id: tmpliderid,
                          lider: tmpusuario,
                          asesor: elem.asesor,
                          equipo: elem.equipo,
                          documento_proyecto: tmpdocumento,
                          fecha_registro: elem.fecha_registro,
                          estado: elem.estado,
                          etapa: elem.etapa,
                          resultado: elem.resultado
                        });

                      }
            
                    });                    
                    
                  });                  
                  
                });                
                
              });
              
            });
            
          });
          
        });
        
      });

    });

    console.log(this.proyecto);
  }

  validateRolUsuarioLiderGestor(id){
    let returnable = false;
    this.proyecto.forEach(element => {
      if(element.id == id){
        if((localStorage.getItem('id_rol') == '1') || (localStorage.getItem('id_usuario') == element.lider_id)){
          returnable = true;
        }else{
          returnable = false;
        }
      }
    });

    return returnable;
  }

  validateRolGestor(etapa, btn){
    if(btn == 'revisar'){
      if((etapa == 'REGISTRO DE PROYECTO') && (localStorage.getItem('id_rol') == '3' || localStorage.getItem('id_rol') == '1')){
        return true;
      }else{
        return false;
      }
    }

    if(btn == 'programar'){
      if(etapa == 'PARA COMITE' && (localStorage.getItem('id_rol') == '3' || localStorage.getItem('id_rol') == '1')){
        return true;
      }else{
        return false;
      }
    }

    if(btn == 'evaluar'){
      if(etapa == 'EN COMITE' && (localStorage.getItem('id_rol') == '3' || localStorage.getItem('id_rol') == '1')){
        return true;
      }else{
        return false;
      }
    }
  }

  validateRolAdmin(){
    if(localStorage.getItem('id_rol') == '1'){
      return true;
    }else{
      return false
    }
  }

  deleteProyecto(id: number){
    this.api.delData('pgp_proyectos/' + id).subscribe((data: any) => {
      if(data == id){
        swal.fire({
          title: 'Exito',
          text: "Proyecto eliminado correctamente.",
          icon: 'success',
        }).then(() => {
          location.reload();
        });
      }else{
        swal.fire({
          title: 'Error',
          text: "Hubo un error al eliminar el proyecto.",
          icon: 'error',
        });  
      }
    });
  }

}
