import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestrotipoproyectos',
  templateUrl: './maestrotipoproyectos.component.html',
  styles: []
})
export class MaestrotipoproyectosComponent implements OnInit {

  tipo_proyectos: any = [];

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataInicial();
  }

  cargaDataInicial(){
    this.api.getData('pgp_tipo_proyectos').subscribe((data: any) => {
      this.tipo_proyectos = data;
    });
  }

  deleteTipoProyecto(id: number){
    this.api.delData('pgp_tipo_proyectos/' + id).subscribe((data: any) => {
      if(data == id){
        swal.fire({
          title: 'Exito',
          text: "Tipo proyecto eliminado correctamente.",
          icon: 'success',
        }).then(() => {
          location.reload();
        });
      }else{
        swal.fire({
          title: 'Error',
          text: "Hubo un error al eliminar el tipo proyecto.",
          icon: 'error',
        });  
      }
    });
  }

}
