import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestronuevoimpacto',
  templateUrl: './maestronuevoimpacto.component.html',
  styles: []
})
export class MaestronuevoimpactoComponent implements OnInit {

  nuevoImpactoId: string = '';
  nuevoImpactoDescripcion: string = '';

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
  }

  crearNuevoImpacto(){
    const dataPost = {
      id: parseInt(this.nuevoImpactoId),
      descripcion: this.nuevoImpactoDescripcion
    };

    console.log(dataPost);

    this.api.getNuevoImpacto(dataPost).subscribe((data: any) => {
      console.log(data);
      if (data.id == this.nuevoImpactoId) {
        swal.fire({
          title: 'Exito',
          text: "Impacto creado correctamente",
          icon: 'success',
        }).then(() => {
          this.router.navigate(['maestros/impacto']);
        });
      } else {
        swal.fire({
          title: 'Error',
          text: "Hubo un error al crear el Impacto, intentelo nuevamente",
          icon: 'error',
        });
      }
    });
  }

}
