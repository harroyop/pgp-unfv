import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestronuevauo',
  templateUrl: './maestronuevauo.component.html',
  styles: []
})
export class MaestronuevauoComponent implements OnInit {

  nuevoUOId: string = '';
  nuevoUODescripcion: string = '';

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
  }

  crearNuevoUO(){
    const dataPost = {
      id: parseInt(this.nuevoUOId),
      descripcion: this.nuevoUODescripcion
    };

    console.log(dataPost);

    this.api.getNuevoUO(dataPost).subscribe((data: any) => {
      console.log(data);
      if (data.id == this.nuevoUOId) {
        swal.fire({
          title: 'Exito',
          text: "Unidad Organizativa creada correctamente",
          icon: 'success',
        }).then(() => {
          this.router.navigate(['maestros/unidades-organizativas']);
        });
      } else {
        swal.fire({
          title: 'Error',
          text: "Hubo un error al crear la unidad organizativa, intentelo nuevamente",
          icon: 'error',
        });
      }
    });
  }

}
