import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  inputUsuario: string = '';
  inputPassword: string = '';
  sessionMsg: string = '';

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = true;
  }

  ngOnInit() {
  }

  loginUser(){
    const formData = new FormData();
    formData.append("usuario", this.inputUsuario);
    formData.append("password", this.inputPassword);

    this.api.getInicioSesion(formData).subscribe((data: any) => {
      console.log(data);
      if (data.codigo == '200') {
        this.sessionMsg = '<i class="fa fa-check" aria-hidden="true"></i> ' + data.message;
        localStorage.setItem('id_usuario', data.data[0].id);
        localStorage.setItem('cod_usuario', data.data[0].cod_usuario);
        localStorage.setItem('nombres', data.data[0].nombres);
        localStorage.setItem('apellidos', data.data[0].apellidos);
        localStorage.setItem('id_unidad_organizativa', data.data[0].id_unidad_organizativa);
        localStorage.setItem('telefono', data.data[0].telefono);
        localStorage.setItem('correo', data.data[0].correo);
        localStorage.setItem('id_rol', data.data[0].id_rol);

        setTimeout(() => {
          window.location.href = "/welcome";
        }, 1500);
      } else {
        this.sessionMsg = '<i class="fa fa-times" aria-hidden="true"></i> ' + data.message;
      }
    });
  }

}
