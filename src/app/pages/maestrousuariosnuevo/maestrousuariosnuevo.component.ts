import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestrousuariosnuevo',
  templateUrl: './maestrousuariosnuevo.component.html',
  styles: []
})
export class MaestrousuariosnuevoComponent implements OnInit {

  uniorg: any = [];
  roles: any = [];

  nuevoUsuarioId: string = '';
  nuevoUsuarioCodUsuario: string = '';
  nuevoUsuarioNombres: string = '';
  nuevoUsuarioApellidos: string = '';
  nuevoUsuarioUnidadOrganizativa: string = '';
  nuevoUsuarioTelefono: string = '';
  nuevoUsuarioCorreo: string = '';
  nuevoUsuarioContrasena: string = '';
  nuevoUsuarioRol: string = '';

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataUnidades();
    this.cargaDataTipoProyectos();
  }

  cargaDataUnidades(){
    this.api.getData('pgp_unidades_organizativas').subscribe((data: any) => {
      this.uniorg = data;
    });
  }

  cargaDataTipoProyectos(){
    this.api.getData('pgp_roles').subscribe((data: any) => {
      this.roles = data;
    });
  }

  crearNuevoUsuario(){
    const dataPost = {
      id: parseInt(this.nuevoUsuarioId),
      cod_usuario: this.nuevoUsuarioCodUsuario,
      nombres: this.nuevoUsuarioNombres,
      apellidos: this.nuevoUsuarioApellidos,
      id_unidad_organizativa: parseInt(this.nuevoUsuarioUnidadOrganizativa),
      telefono: this.nuevoUsuarioTelefono,
      correo: this.nuevoUsuarioCorreo,
      contrasena: this.nuevoUsuarioContrasena,
      id_rol: parseInt(this.nuevoUsuarioRol)
    };

    console.log(dataPost);

    this.api.getNuevoUsuario(dataPost).subscribe((data: any) => {
      console.log(data);
      if (data.id == this.nuevoUsuarioId) {
        swal.fire({
          title: 'Exito',
          text: "Usuario creado correctamente",
          icon: 'success',
        }).then(() => {
          this.router.navigate(['maestros/usuarios']);
        });
      } else {
        swal.fire({
          title: 'Error',
          text: "Hubo un error al crear el usuario, intentelo nuevamente",
          icon: 'error',
        });
      }
    });
  }

}
