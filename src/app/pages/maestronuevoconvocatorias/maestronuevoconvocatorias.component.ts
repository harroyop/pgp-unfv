import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-maestronuevoconvocatorias',
  templateUrl: './maestronuevoconvocatorias.component.html',
  styles: []
})
export class MaestronuevoconvocatoriasComponent implements OnInit {

  unidades: any = [];
  tipo_proyectos: any = [];

  nuevaConvocatoriaId: string = '';
  nuevaConvocatoriaDescripcion: string = '';
  nuevaConvocatoriaIdTipoProyecto: string = '';
  nuevaConvocatoriaIdUnidad: string = '';
  nuevaConvocatoriaVigencia: string = '';

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) {
    this.appComponent.login = false;
  }

  ngOnInit() {
    this.cargaDataUnidades();
    this.cargaDataTipoProyectos();
  }

  cargaDataUnidades(){
    this.api.getData('pgp_unidades').subscribe((data: any) => {
      this.unidades = data;
    });
  }

  cargaDataTipoProyectos(){
    this.api.getData('pgp_tipo_proyectos').subscribe((data: any) => {
      this.tipo_proyectos = data;
    });
  }

  crearNuevaConvocatoria(){
    const dataPost = {
      id: parseInt(this.nuevaConvocatoriaId),
      descripcion: this.nuevaConvocatoriaDescripcion,
      id_tipo_proyecto: parseInt(this.nuevaConvocatoriaIdTipoProyecto),
      id_unidad: parseInt(this.nuevaConvocatoriaIdUnidad),
      vigencia: this.nuevaConvocatoriaVigencia
    };

    console.log(dataPost);

    this.api.getNuevaConvocatoria(dataPost).subscribe((data: any) => {
      console.log(data);
      if (data.id == this.nuevaConvocatoriaId) {
        swal.fire({
          title: 'Exito',
          text: "Convocatoria creada correctamente",
          icon: 'success',
        }).then(() => {
          this.router.navigate(['maestros/convocatorias']);
        });
      } else {
        swal.fire({
          title: 'Error',
          text: "Hubo un error al crear la convocatoria, intentelo nuevamente",
          icon: 'error',
        });
      }
    });
  }

}
