import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpEventType, HttpClient } from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-registroproyectos',
  templateUrl: './registroproyectos.component.html',
  styles: []
})
export class RegistroproyectosComponent implements OnInit {

  convocatorias: any = [];
  unidades: any = [];
  lineas: any = [];
  categorias: any = [];
  impactos: any = [];

  nuevoProyectoId: string = (Math.floor(Math.random() * (999 - 1)) + 1).toString();
  nuevoProyectoCodProyecto: string = '2021' + (Math.floor(Math.random() * (999 - 1)) + 1).toString();
  nuevoProyectoIdConvocatoria: string = '';
  nuevoProyectoNombre: string = '';
  nuevoProyectoDescripcion: string = '';
  nuevoProyectoIdUnidad: string = '';
  nuevoProyectoIdLinea: string = '';
  nuevoProyectoIdCategoria: string = '';
  nuevoProyectoIdImpacto: string = '';
  nuevoProyectoLider: string = localStorage.getItem('id_usuario');
  nuevoProyectoLiderNombre: string = localStorage.getItem('nombres') + ' ' + localStorage.getItem('apellidos');
  nuevoProyectoAsesor: string = '';
  nuevoProyectoEquipo: any = [];
  nuevoProyectoIdDocumentoProyecto: string = '0';
  nuevoProyectoFechaRegistro: string = '';
  nuevoProyectoEstado: string = 'REGISTRADO';
  nuevoProyectoEtapa: string = 'REGISTRO DE PROYECTO';
  nuevoProyectoResultado: string = 'PENDIENTE';

  modalRef: BsModalRef;

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent, private modalService: BsModalService, private http: HttpClient) {
    this.appComponent.login = false;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  ngOnInit() {
    this.cargaDataConvocatorias();
    this.cargaDataUnidades();
    this.cargaDataCategorias();
    this.cargaDataImpactos();
  }

  cargaDataConvocatorias(){
    this.api.getData('pgp_convocatorias').subscribe((data: any) => {
      this.convocatorias = data;
    });
  }

  agregaParticipanteEquipo(){
    let nombres = document.getElementById('agregaEquipoNombre') as HTMLInputElement;
    let codigo = document.getElementById('agregaEquipoCodigo') as HTMLInputElement;
    let tipo = document.getElementById('agregaEquipoTipo') as HTMLInputElement;
    let area = document.getElementById('agregaEquipoArea') as HTMLInputElement;

    this.nuevoProyectoEquipo.push({
      nombre: nombres.value,
      codigo: codigo.value,
      tipo: tipo.value,
      area: area.value
    });

    nombres.value = '';
    codigo.value = '';
    tipo.value = '';
    area.value = '';
  }

  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }
    let fileToUpload = <File>files[0];
    let randomId = Math.floor(Math.random() * (999 - 1)) + 1;
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    this.http.post('https://localhost:44347/api/tools/uploadfile', formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          console.log('Progress: ', Math.round(100 * event.loaded / event.total));
        else if (event.type === HttpEventType.Response) {

          const dataPost = {
            id: randomId,
            descripcion: 'Archivo de Proyecto: ' + this.nuevoProyectoNombre,
            ruta: 'https://localhost:44347/uploads/' + fileToUpload.name
          };
      
          this.api.getNuevoDocumentoProyectos(dataPost).subscribe((data: any) => {
            if (data.id == randomId) {
              swal.fire({
                title: 'Exito',
                text: 'Archivo cargado correctamente',
                icon: 'success',
              });
      
              this.nuevoProyectoIdDocumentoProyecto = data.id;
            }else{
              swal.fire({
                title: 'Error',
                text: 'Error al cargar archivo',
                icon: 'error',
              });
            }
          });
          
        }
      });
  }

  cargaDataUnidades(){
    this.api.getData('pgp_unidades').subscribe((data: any) => {
      this.unidades = data;
    });
  }

  actualizaDataLineas(){
    const formData = new FormData();
    formData.append("id_unidad", this.nuevoProyectoIdUnidad);

    this.api.getLineaxUnidad(formData).subscribe((data: any) => {
      console.log(data);
      if (data.codigo == '200') {
        this.lineas = data.data;
        document.getElementById('nuevoProyectoIdLinea').removeAttribute("disabled");
      } else {
        this.lineas = [];
        swal.fire({
          title: 'Error',
          text: data.message,
          icon: 'error',
        });
      }
    });
  }

  cargaDataCategorias(){
    this.api.getData('pgp_categoria').subscribe((data: any) => {
      this.categorias = data;
    });
  }

  cargaDataImpactos(){
    this.api.getData('pgp_impacto').subscribe((data: any) => {
      this.impactos = data;
    });
  }

  borraParticipanteEquipo(codigo){

    for(var i = 0; i < this.nuevoProyectoEquipo.length; i++){
      if(this.nuevoProyectoEquipo[i].codigo == codigo){
        this.nuevoProyectoEquipo.splice(i, 1);
        
        swal.fire({
          title: 'Exito',
          text: 'Miembro del equipo eliminado correctamente.',
          icon: 'success',
        });
      }
    }
  }

  crearNuevoProyecto(){
    const timeElapsed = Date.now();
    const today = new Date(timeElapsed);
    let fechaActual = today.toLocaleDateString();

    const dataPost = {
      id: parseInt(this.nuevoProyectoId),
      cod_proyecto: this.nuevoProyectoCodProyecto,
      id_convocatoria: parseInt(this.nuevoProyectoIdConvocatoria),
      nombre: this.nuevoProyectoNombre,
      descripcion: this.nuevoProyectoDescripcion,
      id_unidad: parseInt(this.nuevoProyectoIdUnidad),
      id_linea: parseInt(this.nuevoProyectoIdLinea),
      id_categoria: parseInt(this.nuevoProyectoIdCategoria),
      id_impacto: parseInt(this.nuevoProyectoIdImpacto),
      lider: parseInt(this.nuevoProyectoLider),
      asesor: this.nuevoProyectoAsesor,
      equipo: JSON.stringify(this.nuevoProyectoEquipo),
      id_documento_proyecto: parseInt(this.nuevoProyectoIdDocumentoProyecto),
      fecha_registro: today,
      estado: this.nuevoProyectoEstado,
      etapa: this.nuevoProyectoEtapa,
      resultado: this.nuevoProyectoResultado
    };

    console.log(dataPost);

    this.api.getNuevoProyecto(dataPost).subscribe((data: any) => {
      console.log(data);
      if (data.id == this.nuevoProyectoId) {
        swal.fire({
          title: 'Exito',
          text: "Proyecto registrado correctamente",
          icon: 'success',
        }).then(() => {
          this.router.navigate(['bandeja-de-ideas']);
        });
      } else {
        swal.fire({
          title: 'Error',
          text: "Hubo un error al crear la categoria, intentelo nuevamente",
          icon: 'error',
        });
      }
    });
  }
}
