import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  urlApi: string = 'https://localhost:44347/api/';

  constructor(private httpClient: HttpClient, private router: Router) { }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      swal.fire({
        title: 'Error',
        text: "Hubo un error al ejecutar tu funcion: " + error.status + ' ' + error.error,
        icon: 'error',
      });  
      
      // console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    
    return throwError('Something bad happened; please try again later.');
  };

  getQuery(query: string) {
    const url = `${ this.urlApi + query }`;
    return this.httpClient.get(url);
  }

  delQuery(query: string) {
    const url = `${ this.urlApi + query }`;
    return this.httpClient.delete(url).pipe(
      catchError(this.handleError)
    );
  }

  updateQuery(query: string, params: any) {
    const url = `${ this.urlApi + query }`;
    return this.httpClient.put(url, params).pipe(
      catchError(this.handleError)
    );
  }

  postQueryWOH(query: string, params: any) {
    const url = `${ this.urlApi + query }`;
    return this.httpClient.post(url, params);
  }

  postQuery(query: string, params: any) {
    const url = `${ this.urlApi + query }`;
    return this.httpClient.post(url, params, {
      headers:{
        'Content-Type':"application/json"
      }
    }).pipe(
      catchError(this.handleError)
    );
  }

  getInicioSesion(data: object) {
    return this.postQueryWOH('tools/login', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getLineaxUnidad(data: object) {
    return this.postQueryWOH('tools/lineaxunidad', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevoTipoProyecto(data: object) {
    return this.postQuery('pgp_tipo_proyectos', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevaUnidad(data: object) {
    return this.postQuery('pgp_unidades', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevoDocumentoProyectos(data: object) {
    return this.postQuery('pgp_documento_proyectos', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevoProyecto(data: object) {
    return this.postQuery('pgp_proyectos', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevoComite(data: object) {
    return this.postQuery('pgp_programacion_comites', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getEvaluaComite(data: object) {
    return this.postQuery('pgp_resultado_comites', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevaCalificacion(data: object) {
    return this.postQuery('pgp_revision_proyectos', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getUpdateProyecto(data: object, id) {
    return this.updateQuery('pgp_proyectos/' + id, data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getUpdateUsuario(data: object, id) {
    return this.updateQuery('pgp_usuarios/' + id, data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevaConvocatoria(data: object) {
    return this.postQuery('pgp_convocatorias', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevoUsuario(data: object) {
    return this.postQuery('pgp_usuarios', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevoRol(data: object) {
    return this.postQuery('pgp_roles', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevaLinea(data: object) {
    return this.postQuery('pgp_lineas', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevoImpacto(data: object) {
    return this.postQuery('pgp_impacto', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevaCategoria(data: object) {
    return this.postQuery('pgp_categoria', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getNuevoUO(data: object) {
    return this.postQuery('pgp_unidades_organizativas', data).pipe(
      map(data => {
        return data;
      })
    );
  }

  getData(data: string) {
    return this.getQuery(data).pipe(
      map(data => {
        return data;
      })
    );
  }

  delData(data: string) {
    return this.delQuery(data).pipe(
      map(data => {
        return data;
      })
    );
  }
}
