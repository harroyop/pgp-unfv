import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {
  usuario: any = [];

  id_usuario: string = localStorage.getItem('id_usuario');
  nombres: string = localStorage.getItem('nombres');
  apellidos: string = localStorage.getItem('apellidos');
  rol: string = localStorage.getItem('id_rol');
  rol_descripcion: string = '';
  newPassword: string = '';

  modalRef: BsModalRef;

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent, private modalService: BsModalService) { }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  ngOnInit() {
    this.getDataUsuarioRol();
    this.getDataUsuario();
  }

  getDataUsuarioRol(){
    this.api.getData('pgp_roles/' + this.rol).subscribe((data: any) => {
      this.rol_descripcion = data.descripcion;
    }); 
  }

  getDataUsuario(){
    this.api.getData('pgp_usuarios/' + this.id_usuario).subscribe((data: any) => {
      this.usuario = data;
    });
  }

  cambiarPassword(){

    this.usuario.contrasena = this.newPassword;

    const dataPost = this.usuario;

    console.log(dataPost);

    this.api.getUpdateUsuario(dataPost, this.id_usuario).subscribe((data: any) => {
      swal.fire({
        title: 'Exito',
        text: "Usuario actualizado correctamente",
        icon: 'success',
      });
    });
  }

  goToLogin(){
    this.router.navigate(['login']);
  }

}
