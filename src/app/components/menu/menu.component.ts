import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styles: []
})
export class MenuComponent implements OnInit {

  rol: string = localStorage.getItem('id_rol');
  rol_admin: boolean = false;
  rol_user: boolean = false;
  rol_gestor: boolean = false;

  constructor(private router: Router, private api: ApiService, private appComponent: AppComponent) { }

  ngOnInit() {
    this.setMenuRolUser();
  }

  setMenuRolUser(){
    this.api.getData('pgp_roles/' + this.rol).subscribe((data: any) => {
      if(data.descripcion == 'ADMINISTRADOR'){
        this.rol_admin = true;
      }

      if(data.descripcion == 'USUARIO'){
        this.rol_user = true;
      }

      if(data.descripcion == 'GESTOR'){
        this.rol_gestor = true;
      }
    }); 
  }

}
