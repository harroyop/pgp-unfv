import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ROUTES } from './app.routes';
import { FormsModule } from '@angular/forms';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MenuComponent } from './components/menu/menu.component';
import { ChatComponent } from './components/chat/chat.component';
import { FooterComponent } from './components/footer/footer.component';
import { WelcomeComponent } from './pages/welcome/welcome.component';
import { LoginComponent } from './pages/login/login.component';
import { BandejaideasComponent } from './pages/bandejaideas/bandejaideas.component';
import { RegistroproyectosComponent } from './pages/registroproyectos/registroproyectos.component';
import { RevisionproyectosComponent } from './pages/revisionproyectos/revisionproyectos.component';
import { ModificoproyectosComponent } from './pages/modificoproyectos/modificoproyectos.component';
import { RevisioncomiteComponent } from './pages/revisioncomite/revisioncomite.component';
import { ResultadoscomiteComponent } from './pages/resultadoscomite/resultadoscomite.component';
import { MaestrocategoriasComponent } from './pages/maestrocategorias/maestrocategorias.component';
import { MaestroconvocatoriasComponent } from './pages/maestroconvocatorias/maestroconvocatorias.component';
import { MaestroimpactoComponent } from './pages/maestroimpacto/maestroimpacto.component';
import { MaestrolineasComponent } from './pages/maestrolineas/maestrolineas.component';
import { MaestrorolesComponent } from './pages/maestroroles/maestroroles.component';
import { MaestrotipoproyectosComponent } from './pages/maestrotipoproyectos/maestrotipoproyectos.component';
import { MaestrounidadesComponent } from './pages/maestrounidades/maestrounidades.component';
import { MaestrounidadesorganizativasComponent } from './pages/maestrounidadesorganizativas/maestrounidadesorganizativas.component';
import { MaestronuevorolComponent } from './pages/maestronuevorol/maestronuevorol.component';
import { MaestronuevauoComponent } from './pages/maestronuevauo/maestronuevauo.component';
import { MaestronuevoimpactoComponent } from './pages/maestronuevoimpacto/maestronuevoimpacto.component';
import { MaestronuevacategoriaComponent } from './pages/maestronuevacategoria/maestronuevacategoria.component';
import { MaestronuevalineaComponent } from './pages/maestronuevalinea/maestronuevalinea.component';
import { MaestronuevaunidadComponent } from './pages/maestronuevaunidad/maestronuevaunidad.component';
import { MaestronuevotipoproyectoComponent } from './pages/maestronuevotipoproyecto/maestronuevotipoproyecto.component';
import { MaestronuevoconvocatoriasComponent } from './pages/maestronuevoconvocatorias/maestronuevoconvocatorias.component';
import { MaestrousuariosComponent } from './pages/maestrousuarios/maestrousuarios.component';
import { MaestrousuariosnuevoComponent } from './pages/maestrousuariosnuevo/maestrousuariosnuevo.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MenuComponent,
    ChatComponent,
    FooterComponent,
    WelcomeComponent,
    LoginComponent,
    BandejaideasComponent,
    RegistroproyectosComponent,
    RevisionproyectosComponent,
    ModificoproyectosComponent,
    RevisioncomiteComponent,
    ResultadoscomiteComponent,
    MaestrocategoriasComponent,
    MaestroconvocatoriasComponent,
    MaestroimpactoComponent,
    MaestrolineasComponent,
    MaestrorolesComponent,
    MaestrotipoproyectosComponent,
    MaestrounidadesComponent,
    MaestrounidadesorganizativasComponent,
    MaestronuevorolComponent,
    MaestronuevauoComponent,
    MaestronuevoimpactoComponent,
    MaestronuevacategoriaComponent,
    MaestronuevalineaComponent,
    MaestronuevaunidadComponent,
    MaestronuevotipoproyectoComponent,
    MaestronuevoconvocatoriasComponent,
    MaestrousuariosComponent,
    MaestrousuariosnuevoComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ModalModule.forRoot(),
    RouterModule.forRoot(ROUTES, {useHash: false}),
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
